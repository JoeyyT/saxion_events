import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { MaterialModule } from './material.module';
import { AppRoutingModule } from './app-routing.module';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { HomeComponent } from './components/home/home.component';
import { TableExampleComponent } from './components/table-example/table-example.component';
import { MapComponent } from './components/map/map.component';
import { LeafletModule } from '@asymmetrik/ngx-leaflet';

@NgModule({
    declarations: [ AppComponent, PageNotFoundComponent, HomeComponent, TableExampleComponent, MapComponent ],
    imports: [ BrowserModule, BrowserAnimationsModule, MaterialModule, AppRoutingModule, LeafletModule.forRoot() ],
    providers: [],
    bootstrap: [ AppComponent ]
})
export class AppModule {}
